import { Controller, Get, PathParams } from "@tsed/common";
import {Summary, Description, Responses,Deprecated, Security, Returns, Docs, ReturnsArray} from "@tsed/swagger";

import { LocationService } from "../services/LocationService";
import { ContinentModel } from "../models/ContinentModel";
import { CountryModel } from "../models/CountryModel";

@Controller("/location")
@Docs('api-v1') 
export class LocationController {

  constructor(private locationService: LocationService) {  }

  @Get("/continents/")
  @Summary("Continentes.")
  @Description("Retornara todos los continentes GeoName")
  @ReturnsArray(ContinentModel)
  async GetContinents(): Promise<ContinentModel[]> {
    return await this.locationService.GetContinents();
  }

  @Get("/countries/:geonameId")
  @Get("/countries/")
  @Summary("Paises")
  @Description("Retornara Todos Paises o filtrado por continente")
  @ReturnsArray(CountryModel)
  async GetCountries(@PathParams("geonameId") geonameId: number): Promise<CountryModel[]>  {
    return await this.locationService.GetCountries(geonameId);
  }

  @Get("/:id")
  async Create(@PathParams("id") id: string): Promise<string> {
    return `This action returns a #${id} dog`;
  }

}