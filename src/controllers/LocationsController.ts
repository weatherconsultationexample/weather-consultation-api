import { Controller, Get, PathParams } from "@tsed/common";

@Controller("/locations")
export class LocationController {

  constructor() {

  }

  @Get("/:id")
  async Create(@PathParams("id") id: string): Promise<void> {
    console.log(`This action returns a #${id} cat`);
  }

}

