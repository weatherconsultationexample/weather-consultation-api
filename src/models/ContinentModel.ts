import { Title } from "@tsed/common";
import { Description, Example } from "@tsed/swagger";

export class ContinentModel {    
    adminName1: string;
    @Example('L')
    fcl: string;
    @Example('parks,area, ...')
    fclName:string;
    @Title('Codigo')
    @Description('Codigo que  de GeoName')
    @Example('CONT')
    fcode:string;
    @Example('continent')
    fcodeName:string;
    @Title('ID GeoName')
    @Description('ID que representa dentro de GeoName')
    @Example('6255146')
    geonameId:number;
    @Example('7.1881')
    lat:string;
    @Example('21.09375')
    lng:string;
    @Example('Africa')
    name:string;
    @Example('1031833000')
    population:number;
    @Example('Africa')
    toponymName:string;
}
