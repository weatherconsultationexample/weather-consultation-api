
export class CountryModel {
    adminCode1: string;
    adminName1: string;
    countryCode: string;
    countryId: string;
    countryName: string;
    fcl: string;
    fclName: string;
    fcode: string;
    fcodeName: string;
    geonameId: number;
    lat: string;
    lng: string;
    name: string;
    population: number;
    toponymName: string;
}