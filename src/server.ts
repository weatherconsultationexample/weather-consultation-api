import {ServerLoader, ServerSettings, GlobalAcceptMimesMiddleware, $log} from "@tsed/common";
import "@tsed/typeorm";
import * as dotenv from "dotenv";
import * as Path from "path";
//import {$log} from "ts-log-debug";
import yn from 'yn';
import GlobalErrorHandlerMiddleware from "./middlewares/GlobalErrorHandlerMiddleware";
//import * as fs from "fs";

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');

if (process.env.NODE_ENV !== "production") {
    // Used only in development to load environment variables from local file.
    dotenv.config();    
}

const rootDir = Path.resolve(__dirname);

@ServerSettings({
    rootDir: rootDir, // optional. By default it's equal to process.cwd()
    acceptMimes: ["application/json"],
    httpPort: process.env.PORT || 8080,
    debug: yn(process.env.DEBUG) || false,
     mount: {
        "/api": `${rootDir}/controllers/**/*{.ts,.js}` // support ts with ts-node then fallback to js
    }, 
    logger: {
        requestFields: ["method", "url", "headers", "body", "query","params", "duration"]
    },
    swagger: [
        {
          path: "/api-docs"
        }
    ]
})

export class Server extends ServerLoader {

    /**
     * This method let you configure the express middleware required by your application to works.
     * @returns {Server}
     */
    public $onMountingMiddlewares(): void|Promise<any> {
    
        const rootDir = Path.resolve(__dirname);

        this
            .use(GlobalAcceptMimesMiddleware)
            .use(GlobalErrorHandlerMiddleware)
            .use(cookieParser())
            .use(compress({}))
            .use(methodOverride())
            .use(bodyParser.json())
            .use(bodyParser.urlencoded({
                extended: true
            }));

        return null;
    }

    public $onReady(){
        $log.debug("Server started...");
        $log.info("Info Server started...");
        console.log('Server started...');
        console.log(`Site start Direction http://localhost:${process.env.PORT}/`);
        console.log(`Site start Swagger http://localhost:${process.env.PORT}/api-docs/`);

    }
   
    public $onServerInitError(err){
        console.log(err);
    }
}

new Server()
    .start()
    .catch((er) => console.error(er));