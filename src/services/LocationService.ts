import { Service } from "@tsed/di";
import * as Geonames from 'geonames.js';
import { ICountry } from "../abstract/geonames/ICountry";
import { ContinentModel } from "../models/ContinentModel";
import { CountryModel } from "../models/CountryModel";

@Service()
export class LocationService {

  private geonames: Geonames;

  constructor() {
    this.geonames = new Geonames({
      username: process.env.GEONAMES || 'nicocatalogna',
      lan: 'en',
      encoding: 'JSON'
    });
  }

  public async GetContinents(): Promise<ContinentModel[]> {

      const result = await this.geonames.children({ geonameId: 6295630}); //Buscar Continentes
      
      return result.geonames as ContinentModel[];
  }

  public async GetCountries(geonameId: number): Promise<CountryModel[]> {

    const result = await this.geonames.children({ geonameId: geonameId});

    return result.geonames as CountryModel[];
  }

}
